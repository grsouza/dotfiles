set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ericcurtin/CurtineIncSw.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" use identation of previous line
set autoindent

" use intelligent indentation for C
set smartindent

set tabstop=2
set shiftwidth=2
set expandtab

set textwidth=120

set t_Co=256

syntax on

set number

set showmatch

set comments=sl:/*,mb:\ *,elx:\ */


set bg=dark
