set background=dark
" Use 14pt Monaco
set guifont=Monaco:h12
" Don't blink cursor on normal mode
set guicursor=n:blinkon0
" Better line-height
set linespace=4
