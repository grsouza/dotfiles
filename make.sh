#!/bin/bash

DOTFILES_DIR="$HOME/dotfiles"
BACKUP_DIR="$HOME/dotfiles_backup"
FILES=".profile .vimrc .vim .zshrc .gvimrc"

if [ ! -d $BACKUP_DIR ]; then
  echo "Creating backup directory at $BACKUP_DIR"
  mkdir $BACKUP_DIR
fi

for file in $FILES; do
  if [ -e $HOME/$file ]; then
    echo "Moving $HOME/$file to $BACKUP_DIR/$file"
    mv $HOME/$file $BACKUP_DIR/$file
  fi
done

echo

for file in $FILES; do
  echo "Linking $DOTFILES_DIR/$file to $HOME/$file"
  ln -s $DOTFILES_DIR/$file $HOME/$file
done
