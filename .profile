
# aliases
alias brewski="brew update && brew upgrade && brew cleanup"

# Flutter
export PATH="$HOME/Source/flutter/bin:$PATH"
